== 1.6 Swift ==

El llenguatge Swift és un nou llenguatge de programació presentat el 2014 per Apple per programar tant per a la plataforma Mac com per a iOS, i està pensat per a substituir el llenguatge ja arcaic i complicat Objective-C. És un llenguatge molt modern que agafa idees de llenguatges més evolucionats com C#, Javascript, Haskell, Python, etc. i les aplica per crear un nou llenguatge de programació molt senzill i intuïtiu.

És un llenguatge creat recentment i que encara està en evolució, ja que s'hi van afegint petits canvis de tant en tant. La primera versió del llenguatge Swift es va presentar el setembre de 2014, i l'abril de 2015 en va aparèixer la versió 1.2. El juny del 2015 Apple va anunciar la versió 2.0.

Està fonamentat sobre una base de C però amb unes característiques que ens ajuden a escriure programes més eficientment i també amb menys errors. A més, algunes d'aquestes característiques ajuden el compilador a generar un codi més ràpid que un programa escrit amb Objective-C, per la qual cosa tot són avantatges. Sens dubte, és el llenguatge del futur de les plataformes d'Apple.

Algunes de les característiques més destacades són:


**Type inference**
Aquesta característica ens permet treballar amb diferents variables sense haver de definir el tipus de cadascuna, perquè el compilador determinarà el tipus depenent del valor amb què s'inicialitzi aquella variable.


**Type safety**
Aquesta característica està relacionada amb Type inference, i permet al compilador conèixer el tipus de variables en temps de compilació i no d'execució, cosa que li permet aplicar una sèrie d'optimitzacions que milloren el rendiment del programa.


**Mutability**

El llenguatge Swift ens obliga a definir, en declarar les nostres variables, si aquestes es poden modificar i, per tant, si seran variables, o, per contra, si no es poden modificar i, per tant, seran constants. Això ajuda el compilador a optimitzar el rendiment de la nostra aplicació.


**Generics**
Permet crear funcions genèriques que funcionen per a qualsevol tipus de variable. Això ens permet una gran flexibilitat per fer funcions molt potents amb molt poc codi.


**Extensions**

Les extensions ens permeten afegir funcions pròpies a qualsevol element de Swift, incloent-hi els tipus més elementals, cosa que ens permet crear una extensió d'un element i utilitzar-la fàcilment sempre que ho necessitem.


Trobem moltes més novetats que ens ajudaran a desenvolupar aplicacions més ràpidament i més fàcilment, com no necessitar més els fitxers //header// (.h), no haver de declarar punters o no haver de posar més ; al final de cada instrucció.

Com veiem, el llenguatge Swift aporta moltes característiques innovadores però que a la vegada ens faciliten la tasca de programar amb un llenguatge molt més natural. Tot això està provocant que estigui tenint una molt bona adopció des que es va presentar. Un dels avantatges principals també és que el llenguatge Swift es pot comunicar fàcilment amb parts de la nostra aplicació escrites en Objective-C i a l'inrevés, la qual cosa ajuda que vagi agafant cada vegada més impuls, ja que és molt fàcil integrar-ho a poc a poc.

Si hem d'aprendre un nou llenguatge de programació per a iOS i estem entre començar amb Swift o Objective-C, és recomanable començar per Swift, ja que és molt més clar i senzill que no pas l'Objective-C. Un cop ja estiguem familiaritzats amb les llibreries de Cocoa Touch i el seu funcionament ens serà més senzill entendre el codi escrit en Objective-C. Malauradament, encara hi ha moltes llibreries i aplicacions fetes en Objective-C, per la qual cosa, encara que Swift sigui el futur, encara haurem d'utilitzar Objective-C durant força temps.


=== 1.6.1 Hello world ===

Tradicionalment es comença ensenyant el missatge "Hello world" per pantalla en començar un nou llenguatge de programació.

En Objective-C el codi necessari seria aquest:

(% style="background-color: rgb(204, 204, 204);" %)#import <stdio.h>(%%)
(% style="background-color: rgb(204, 204, 204);" %)#import <Foundation/Foundation.h>(%%)
(% style="background-color: rgb(204, 204, 204);" %) (%%)
(% style="background-color: rgb(204, 204, 204);" %)int main(void)(%%)
(% style="background-color: rgb(204, 204, 204);" %){(%%)
(% style="background-color: rgb(204, 204, 204);" %)    NSLog(@"Hello, world!\n");(%%)
(% style="background-color: rgb(204, 204, 204);" %)    return 0;(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

En Swift només necessitem:

(% style="background-color: rgb(204, 204, 204);" %)print"Hello, world!")

(% style="font-size: 14px; /* background-color: rgb(245, 245, 245); */" %)Com veiem, l'exemple de Swift és infinitament més senzill i clar que no pas el codi d'Objective-C.


=== 1.6.2 Sintaxi ===

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;" %)
Per fer proves i aprendre la sintaxi del llenguatge Swift, la millor manera és utilitzar els Playgrounds, una interfície de programació d'Apple que ens permet veure a temps real els resultats del que anem fent; és perfecte per a fer proves i experiments d'una forma molt fàcil.

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;" %)
Per crear-ne un de nou podem obrir el programa Xcode i escollir l'opció New > Playground.

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;" %)
Per defecte veurem aquest codi:

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;" %)
(% style="font-size: 14px; background-color: rgb(204, 204, 204);" %)import UIKit(%%)
(% style="font-size: 14px; background-color: rgb(204, 204, 204);" %)var str = "Hello, playground"

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;" %)
La instrucció import UKit ens permet fer referència al //framework //UIKit i, per tant, referenciar els seus elements. De totes maneres, als exemples que farem no ho necessitarem; per tant, ho podem obviar.

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;" %)
La segona instrucció ens defineix una variable anomenada str amb el text "Hello, playground". No cal que definim el tipus de variable com en altres llenguatges, ja que es dedueix de la seva inicialització. En aquest cas, la variable str serà del tipus String perquè s'inicialitza amb un String.


**Variables bàsiques**

Existeixen diferents tipus de variables a Swift. Les més comunes serien String, Int, Double i Bool.

El tipus String per a cadenes de caràcters com "Hello", "Hola" (s'indiquen entre cometes i, al contrari d'Objective-C, no s'ha de posar el símbol @ davant).
La variable de tipus Int per als enters, com 1, 2, 3, 4.
La variable de tipus Double per als nombres amb decimals, com 1,3, 0,5.
El tipus Bool per a indicar cert o fals. S'indica amb "true" o "false".

A Swift les variables es declaren amb "var" davant i les constants, amb "let" davant. Això ajuda el compilador a diferenciar correctament entre variables i constants i permet obtenir un millor rendiment.

Cal aclarir que les constants no cal que tinguin valor en temps de compilació, però només els podrem donar valor un sol cop. A més, si són objectes, tot i ser constants, podrem modificar els seus paràmetres, però el que no podrem és assignar-li un nou objecte.

Aquest exemple ens donarà un error, ja que estarem assignant dues vegades un valor a una constant:

(% style="background-color: rgb(204, 204, 204);" %)let str2 = ""(%%)
(% style="background-color: rgb(204, 204, 204);" %)str2 = "valor"

En canvi, així sí que serà correcte:

(% style="background-color: rgb(204, 204, 204);" %)let str2:String(%%)
(% style="background-color: rgb(204, 204, 204);" %)str2 = "valor"

A la primera instrucció indiquem que str serà del tipus String i constant però no li assignem cap valor, per la qual cosa a la segona instrucció, quan li assignem el valor, no ens dóna cap error.

Swift, com hem comentat anteriorment, utilitza Type inference per a esbrinar el tipus de les nostres variables. Sempre podem definir-les si volem, però no és necessari. De vegades, per això, ens pot ajudar especificar el tipus que volem.

En aquest exemple s'executarà correctament:

(% style="background-color: rgb(204, 204, 204);" %)let valor1:Double = 1(%%)
(% style="background-color: rgb(204, 204, 204);" %)let valor2:Double = 3.4(%%)
(% style="background-color: rgb(204, 204, 204);" %)let suma:Double = valor1 + valor2

En canvi, si no definim els tipus, donarà error, ja que el valor 1 s'inicialitzarà com un enter Int i no pas com un nombre real Double:

(% style="background-color: rgb(204, 204, 204);" %)let valor1 = 1(%%)
(% style="background-color: rgb(204, 204, 204);" %)let valor2 = 3.4(%%)
(% style="background-color: rgb(204, 204, 204);" %)let suma = valor1 + valor2

Per defecte, Swift declara els nombres reals com a Double i no pas com a Float. Double té més precisió que no pas Float, i així, per defecte, Swift l'agafa com a opció predeterminada.

Podríem solucionar el problema anterior fent un //cast//, és a dir, transformar d'un tipus a un altre. Per fer un //cast //hem de posar la variable entre parèntesis i indicar el tipus de variable en la qual volem transformar-la ("Tipus(variable)"):

(% style="background-color: rgb(204, 204, 204);" %)let valor1 = 1(%%)
(% style="background-color: rgb(204, 204, 204);" %)let valor2 = 3.4(%%)
(% style="background-color: rgb(204, 204, 204);" %)let suma = Double(valor1) + valor2

Quant al tipus String, podem unir diferents String i variables de la manera següent:

(% style="background-color: rgb(204, 204, 204);" %)let cadena1 = "Hola "(%%)
(% style="background-color: rgb(204, 204, 204);" %)let cadena2 = "són les "
let hora = 14(%%)
(% style="background-color: rgb(204, 204, 204);" %)let resultat = cadena1 + cadena2 + "\(hora) hores"

Amb el símbol + podem unir diferents cadenes de text, i dins una cadena de text, si afegim "\(variable)" podem mostrar també variables dins la cadena de text.

Si estem utilitzant Playgrounds veurem el resultat a la part dreta de la pantalla. Si no, per mostrar-ho per la consola afegiríem l'opció:

(% style="background-color: rgb(204, 204, 204);" %)print(resultat)

La funció print ens permet imprimir una variable o una cadena de caràcters per la consola. Ens serà indispensable a l'hora de programar i poder esbrinar si el codi s'està executant correctament. Ens imprimirá la variable seguida per un salt de linia.

Nota: En versions anteriors de Swift teniem //println//, que imprimia la variable seguida per un salt de linia, y //print//, que només imprimia la variable. A Swift 2.0 s'han unificat aquestes dues funcions en la nova //print.// Si no volem que ens afegeixi el salt de linia al final podem utilitzar el paràmetre opcional //terminator// per a especificar què volem que ens afegeixi al final, per exemple una cadena buida, així:

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6px;" %)
(% style="font-size: 14px; background-color: rgb(204, 204, 204);" %)print(resultat, terminator: "")

(((

)))

**Array**

Altres tipus d'objectes amb què podem treballar són els //arrays//, agrupacions d'altres tipus de variables. En Swift només podem tenir //arrays //on tots els elements són del mateix tipus.

Amb Swift els //arrays //són instàncies de la classe Array i els podem declarar de maneres diferents, totes dues són equivalents:

(% style="background-color: rgb(204, 204, 204);" %)var animals = ["dog", "cat", "horse"](%%)
(% style="background-color: rgb(204, 204, 204);" %)var animals:[String] = ["dog", "cat", "horse"]

Per accedir a un element és molt fàcil, ja que hi podem accedir directament amb els claudàtors [ ] indicant l'element en qüestió:

(% style="background-color: rgb(204, 204, 204);" %)let firstAnimal = animals[0]

Per declarar un //array //de //strings //sense cap element ho faríem de la manera següent:

(% style="background-color: rgb(204, 204, 204);" %)var animals = [String]()


**Dictionary**

A més de la classe Array, també tenim la classe Dictionary, que ens permet guardar valors indexats per una clau.

Per crear diccionaris la sintaxi seria d'aquesta manera:

(% style="background-color: rgb(204, 204, 204);" %)var animalSize = [(%%)
(% style="background-color: rgb(204, 204, 204);" %)"dog":"small",(%%)
(% style="background-color: rgb(204, 204, 204);" %)"horse":"big"(%%)
(% style="background-color: rgb(204, 204, 204);" %)]

(% style="background-color: rgb(204, 204, 204);" %)var animalSize:[String:String] = [(%%)
(% style="background-color: rgb(204, 204, 204);" %)"dog":"small",(%%)
(% style="background-color: rgb(204, 204, 204);" %)"horse":"big"(%%)
(% style="background-color: rgb(204, 204, 204);" %)]

Podem accedir a la informació d'un diccionari de manera molt semblant com ho fem amb un //array//, però passant-li la clau que desitgem:

(% style="background-color: rgb(204, 204, 204);" %)let dogSize = animalSize["dog"]

I per declarar un diccionari sense elements ho podríem fer així:

(% style="background-color: rgb(204, 204, 204);" %)var animalSize = [String:String]()

== 1.6.3 //Loops //i flux ==

En Swift, de la mateixa manera que la majoria de llenguatges de programació, disposem de diversos //loops //i condicions que podem utilitzar per a realitzar els nostres algorismes.

El condicional "if" en Swift incorpora algunes millores que ens ajudaran a escriure menys i a estalviar-nos alguns //bugs //habituals.

Per començar, és obligatori obrir i tancar claus. Això evita problemes d'altres llenguatges quan hi ha condicions amb claus i d'altres sense.

No és necessari utilitzar parèntesis en les condicions.

Un exemple:

(% style="background-color: rgb(204, 204, 204);" %)let boolVariable = false(%%)
(% style="background-color: rgb(204, 204, 204);" %)let boolVariable2 = false(%%)
(% style="background-color: rgb(204, 204, 204);" %)if boolVariable == true{(%%)
(% style="background-color: rgb(204, 204, 204);" %)    if boolVariable2 == true{(%%)
(% style="background-color: rgb(204, 204, 204);" %)    }(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

La condició ha de retornar explícitament un booleà, per la qual cosa no podem posar assignacions com a condició o variables que no siguin booleans.
Això és molt pràctic perquè ens estalvia //bugs //on hem posat = en comptes de ==.

Això ens donarà error:

(% style="background-color: rgb(204, 204, 204);" %)if boolVariable = true{(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

L'altre condicional amb què ens trobem és el //switch//, també amb moltes millores interessants. Algunes de les més destacades és que no necessitarem afegir //break// a cada una de les seves opcions per a evitar entrar a la resta d'opcions. Podrem comparar tota mena de variables, no només enters, podem afegir més d'una condició i altres opcions més avançades en les quals ara no entrarem.

Un exemple seria:

(% style="background-color: rgb(204, 204, 204);" %)let animal = "dog"(%%)
(% style="background-color: rgb(204, 204, 204);" %)var animalSound:String(%%)
(% style="background-color: rgb(204, 204, 204);" %)switch animal {(%%)
(% style="background-color: rgb(204, 204, 204);" %)case "dog":(%%)
(% style="background-color: rgb(204, 204, 204);" %)   animalSound = "Bup"(%%)
(% style="background-color: rgb(204, 204, 204);" %)case "cat", "cat-small":(%%)
(% style="background-color: rgb(204, 204, 204);" %)    let animalSound = "Mèu"(%%)
(% style="background-color: rgb(204, 204, 204);" %)default:(%%)
(% style="background-color: rgb(204, 204, 204);" %)    animalSound = "no idea"(%%)
(% style="background-color: rgb(204, 204, 204);" %)}(%%)
(% style="background-color: rgb(204, 204, 204);" %)print(animalSound)

(% style="font-size: 14px; /* background-color: rgb(245, 245, 245); */" %)A part dels condicionals trobem els //loops//. Com en molts altres llenguatges, tenim el for, el for-in, el while i el repeat-while.

A continuació trobem un exemple del for-in, que és el més habitual d'utilitzar.

(% style="background-color: rgb(204, 204, 204);" %)let colors = ["red","blue","yellow"]

(% style="background-color: rgb(204, 204, 204);" %)for color in colors{(%%)
(% style="background-color: rgb(204, 204, 204);" %)    print("I like color:\(color)")(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

Dos exemples més del //loop //for:

(% style="background-color: rgb(204, 204, 204);" %)for counter in 0..<2 {(%%)
(% style="background-color: rgb(204, 204, 204);" %)    print(counter)(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

Aquest primer //loop //s'executarà dues vegades, ja que els símbols ..< indiquen que no s'executi quan valgui 2:

(% style="background-color: rgb(204, 204, 204);" %)for counter in 0...2 {(%%)
(% style="background-color: rgb(204, 204, 204);" %)    print(counter)(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

Aquest segon //loop //s'executarà tres vegades, ja que també s'executarà pel valor 2.

La sintaxi és molt clara, atès que tampoc no necessitem posar cap parèntesi ni especificar el tipus de variable, ja que en aquest cas Swift ja sap què és un //array //de //strings//.

== 1.6.4 //Optionals// ==

Una de les característiques més importants de Swift és el concepte d'//optional//. Els //optionals //ens permeten gestionar de forma correcta els casos en què un objecte o variable no conté cap valor.

Els //optionals //tenen relació amb el tractament que es feia als objectes en Objetive-C quan en crear-se sense valor tenien un valor de nil. En Swift això també s'aplica a les variables i no només als objectes.

Els //optionals //ens ajuden a especificar millor el comportament del nostre codi, ja que ens permeten definir si una variable ha de tenir un valor o no.

Per declarar un //optional //només hem d'afegir un ? després del tipus:

(% style="background-color: rgb(204, 204, 204);" %)var variable:Int?(%%)
(% style="background-color: rgb(204, 204, 204);" %)variable = 20(%%)
(% style="background-color: rgb(204, 204, 204);" %)variable = nil

Aquest codi ens funcionarà sense problemes, mentre que si el tipus fos Int, no li podríem assignar nil.

L'acció de transformar una variable //optional //en una variable no //optional// s'anomena //unwrap//, i la seva sintaxi habitual és:

(% style="background-color: rgb(204, 204, 204);" %)var optionalAge:Int? = 20(%%)
(% style="background-color: rgb(204, 204, 204);" %)print(optionalAge)(%%)
(% style="background-color: rgb(204, 204, 204);" %)if let age = optionalAge{(%%)
(% style="background-color: rgb(204, 204, 204);" %)    print(age)(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

Si executem aquest codi, veurem que la primera vegada ens mostra en pantalla "Optional(20)", ja que estem tractant amb una variable //optional//. Si volem accedir al valor real, l'haurem de desembolicar o fer un //unwrap //abans.

Una segona manera de fer //unwrap //és posant un signe !.

Un exemple seria el següent:

(% style="background-color: rgb(204, 204, 204);" %)var optionalAge:Int? = 20(%%)
(% style="background-color: rgb(204, 204, 204);" %)if optionalAge != nil{(%%)
(% style="background-color: rgb(204, 204, 204);" %)    print(optionalAge!)(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

El que fem és assegurar-nos abans que l'//optional// no sigui nil, i llavors fem l'//unwrap// amb el signe d'exclamació.

Si fem un //unwrap //sobre un objecte buit o nil, provocarem un error i el programa es tancarà. Haurem d'estar segurs abans de fer un //unwrap //amb el signe ! que realment hi hagi un valor.

== 1.6.5 Funcions i //closures// ==

Per declarar una funció utilitzem la paraula //func//. Les funcions en Swift, com veurem, tenen una sèrie de peculiaritats que podem utilitzar. Algunes coses que podem fer són acollir una funció dins d'una altra, passar com a paràmetre una altra funció, passar un nombre de paràmetres indefinit, retornar més d'un valor, etc. Algunes d'aquestes característiques vénen heretades dels que s'anomenen //blocks //d'Objective-C, que són petits fragments de codi que podem declarar i executar en el moment que vulguem.

En Swift s'ha agafat aquest concepte i s'ha aplicat també a les funcions. En Swift els //blocks //d'Objective-C s'anomenen //closures//. Com veurem, els //closures //no deixen de ser com funcions però sense declarar el nom de la funció, i les funcions no deixen de ser //closures //amb nom.

Un exemple de funció:

(% style="background-color: rgb(204, 204, 204);" %)func sum(value1: Int, value2: Int) -> Int {(%%)
(% style="background-color: rgb(204, 204, 204);" %)    return value1+value2(%%)
(% style="background-color: rgb(204, 204, 204);" %)}(%%)
(% style="background-color: rgb(204, 204, 204);" %)let resultat = sum(4, value2: 5)(%%)
(% style="background-color: rgb(204, 204, 204);" %)print(resultat)

En declarar una funció, com podem veure, definim el tipus de paràmetres que rep entre els parèntesis i el tipus que retorna, si en retorna cap.

A l'exemple següent veiem com seria una funció que sumés amb un nombre il·limitat de paràmetres:

(% style="background-color: rgb(204, 204, 204);" %)func sumAll(numbers: Int...) -> Int {(%%)
(% style="background-color: rgb(204, 204, 204);" %)    var total: Int = 0(%%)
(% style="background-color: rgb(204, 204, 204);" %)    for number in numbers {(%%)
(% style="background-color: rgb(204, 204, 204);" %)        total += number(%%)
(% style="background-color: rgb(204, 204, 204);" %)    }(%%)
(% style="background-color: rgb(204, 204, 204);" %)    return total(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

(% style="background-color: rgb(204, 204, 204);" %)let resultat = sumAll(1,2,3,4)

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; line-height: 19.6px; text-align: start; background-color: rgb(204, 204, 204);" %)print(resultat)

Com podem veure, la sintaxi és molt clara, ja que només hem d'afegir ... darrere del paràmetre que volem repetir. Només funciona per l'últim paràmetre.

Les //closures //tenen una sintaxi que pot semblar una mica més complicada. Un exemple de //closure //seria el mètode següent, que ens ordena una sèrie de noms.

Una //closure //que ens realitzi la mateixa funció que la funció //sum //que hem fet abans es declararia d'aquesta manera:

(% style="background-color: rgb(204, 204, 204);" %){(value1: Int, value2: Int) -> Int in(%%)
(% style="background-color: rgb(204, 204, 204);" %)    return value1+value2(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

Com podem observar, la sintaxi és semblant a la declaració d'una funció però canviant les claus de posició, eliminant el nom i afegint la paraula //in//.

Les //closures //tenen moltes coses que ens poden ajudar. Una de les més importants és que hi ha funcions que les reben com a paràmetre. Un exemple és la funció //sorted//, a la qual podem passar un //array //d'objecte, i com a segon paràmetre una //closure //que ens defineix el mètode d'ordenació a utilitzar:

(% style="background-color: rgb(204, 204, 204);" %)let names = ["Joan", "Albert", "Pere", "Marta", "Olga"]

(% style="background-color: rgb(204, 204, 204);" %)let ordered = names.sort(
    {
        (s1: String, s2: String) -> Bool in(%%)
(% style="background-color: rgb(204, 204, 204);" %)        return s1 < s2(%%)
(% style="background-color: rgb(204, 204, 204);" %)    }
)

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; line-height: 19.6px; text-align: start; background-color: rgb(204, 204, 204);" %)print(ordered)

== 1.6.6 Classes ==

Les definim amb la paraula //class//. Podem definir variables, així com definir constructors, amb la paraula //init//, i destructors de la classe amb la paraula //deinit//. Algunes peculiaritats són que també podem establir funcions que es cridin quan modifiquem una variable, abans o després que es canviï. Com en altres llenguatges, podem definir diferents tipus de permisos per a accedir-hi. En Swift en tenim tres: //public//, //private //i //internal//. Per defecte, si no s'especifica, es declaren com a //internal//, que permet l'accés sempre que es treballi al mateix mòdul o //framework//.

Un exemple de classe seria el següent:

(% style="background-color: rgb(204, 204, 204);" %)class Vehicle {(%%)
(% style="background-color: rgb(204, 204, 204);" %)    var numberOfWheels = 0(%%)
(% style="background-color: rgb(204, 204, 204);" %)    (%%)
(% style="background-color: rgb(204, 204, 204);" %)    init(numberOfWheels:Int){(%%)
(% style="background-color: rgb(204, 204, 204);" %)        self.numberOfWheels = numberOfWheels(%%)
(% style="background-color: rgb(204, 204, 204);" %)    }(%%)
(% style="background-color: rgb(204, 204, 204);" %)    (%%)
(% style="background-color: rgb(204, 204, 204);" %)    func description() -> String {(%%)
(% style="background-color: rgb(204, 204, 204);" %)        return "This vehicle has \(numberOfWheels) wheels."(%%)
(% style="background-color: rgb(204, 204, 204);" %)    }(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

(% style="background-color: rgb(204, 204, 204);" %)let car = Vehicle(numberOfWheels: 2)(%%)
(% style="background-color: rgb(204, 204, 204);" %)car.numberOfWheels = 4(%%)
(% style="background-color: rgb(204, 204, 204);" %)print(car.description())

En Swift no cal heretar de cap //object //com en Objective-C, que s'hereta d'NSObject si no hi ha cap classe de la qual heretem.

A la declaració de la classe, dins les claus establirem les propietats de la classe i després els constructors, destructors o mètodes que necessitem declarar.

== 1.6.7 Conclusions ==

Swift ha millorat en molts aspectes el ja arcaic llenguatge de programació Objective-C, hi ha afegit moltes novetats i l'ha fet molt flexible i amb característiques molt innovadores, per la qual cosa programar apps per iOS ara és molt més intuïtiu i senzill.

En aquest breu espai de temps de vida de Swift ja s'han creat moltíssimes llibreries 100% escrites en Swift i de molt bona qualitat. Sens dubte, el llenguatge Swift ha vingut per quedar-se.

Hi ha moltes altres coses de Swift que no hem pogut comentar; per tant, us animo a continuar aprenent les seves característiques amb la informació oficial d'Apple.

Podeu trobar més informació de Swift a:

- Vídeos de la WWDC.
- Apple disposa de diversos //ebooks //a la seva app iBook que tracten Swift.
- https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/index.html
