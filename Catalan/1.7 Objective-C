== 1.7 Introducció Objective-C ==

Objective-C és històricament el llenguatge associat a la programació de programes per als sistemes operatius d'Apple, almenys des de l'aparició de Mac OS X.

L'Objective-C és un llenguatge evolucionat del C, de la mateixa manera que C++. Tots dos són llenguatges orientats a objectes (OOP), i el seu principal objectiu és la reutilització de codi i permetre crear programes més complexos amb estructures més senzilles.

L'Objective-C, a més de la sintaxi, té algunes diferències estructurals amb C++, ja que han evolucionat de manera diferent. L'Objective-C ha rebut molta influència d'un altre llenguatge anomenat Smalltalk.

L'Objective-C és totalment compatible amb el codi C. Objective-C suposa només una capa per sobre de C; així, podrem utilitzar llibreries, tipus de variables i funcions de C sense problemes. (% style="font-size: 14px; /* background-color: rgb(245, 245, 245); */" %)Per aprofitar els avantatges d'un llenguatge orientat a objectes no s'utilitza gaire codi en C, tot i que aquesta característica ens pot ser útil per a integrar codi C quan sigui necessari.

Una de les característiques d'Objective-C és l'enviament de missatges. Quan es crida un mètode en altres llenguatges el nom del mètode estableix un punt fix a executar. En Objective-C el mètode a cridar es resol en temps d'execució, per la qual cosa no està assegurada la resposta. Podem expressar, per tant, que quan es crida un mètode s'envia un missatge, ja que no tenim assegurada la resposta.

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;" %)
Podeu trobar més informació a:

(% style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;" %)
[[http:~~/~~/en.wikipedia.org/wiki/Objective-C>>http://en.wikipedia.org/wiki/Objective-C||style="font-family: sans-serif; font-size: 14px; font-style: normal; text-align: start; line-height: 19.6000003814697px;"]]
[[http:~~/~~/en.wikipedia.org/wiki/Object-oriented_programming>>http://en.wikipedia.org/wiki/Object-oriented_programming||style="font-size: 14px;"]]


=== 1.7.1 Nomenclatura ===

Existeix una nomenclatura de noms a Objective-C que ajuda a identificar variables, mètodes, classes, etc.

Les classes comencen sempre amb una lletra majúscula, i si està formada per més d'una paraula, les paraules següents també comencen per una lletra majúscula. Els noms de classes acostumen a anar en singular.

Exemples de noms de classes: AddressBook, MultipleContact, Contact.

Quant a les variables, aquestes comencen sempre amb una lletra minúscula o guió baix. Si està formada per més d'una paraula, s'acostuma a posar en majúscula la segona paraula o un guió baix entre elles.

Exemples de noms de variables: counter, currentValue, contact_name.

I per últim, trobem els mètodes, que comencen amb una paraula en minúscula que ha de ser una acció, i si tenen més paraules, es posen en majúscula a continuació.

Exemples de mètodes: print, addContact, removeList.


=== 1.7.2 Declaració de variables ===

Abans d'entrar de ple en la sintaxi del llenguatge Objective-C és important notar que Objective-C utilitza punters quan treballa amb objectes. S'ha de tenir en compte, quan es crea un objecte o quan se'n passa un per paràmetre, indicar que es tracta d'un objecte amb el caràcter *. No entrarem a explicar el funcionament dels punters, simplement hem de recordar-nos d'afegir el caràcter * quan declarem objectes o rebem un objecte a una funció.

Primer de tot observem com declarem un objecte. Fixem-nos en el * perquè es tracta d'un objecte.

(% style="background-color: rgb(204, 204, 204);" %)Class *object;

Per contra, per crear una variable bàsica de C no caldrà el *.

(% style="background-color: rgb(204, 204, 204);" %)int a;

Per cridar un mètode o enviar un missatge a un objecte, la sintaxi és la següent:

(% style="background-color: rgb(204, 204, 204);" %)[objecte metode];

El que es fa és indicar primer el receptor i després indicar el mètode que es vol cridar, tot entre claudàtors.

El receptor pot ser tant un objecte com una classe, ja que també existeixen els mètodes de classes, anomenats mètodes //static //en altres llenguatges. En aquest cas la crida seria:

(% style="background-color: rgb(204, 204, 204);" %)[Classe metode];

Tornant a la creació d'objectes amb Objective-C, és important entendre que per treballar amb objectes no en tenim prou a declarar-los sinó que haurem de demanar també memòria i inicialitzar-los. Això serien tres instruccions:

La primera, la declaració d'un objecte:

(% style="background-color: rgb(204, 204, 204);" %)Class *object;

La segona, demanar memòria amb la funció //alloc//. Aquest és el mètode que s'encarrega de demanar la memòria necessària per a poder guardar l'objecte i ens retorna un objecte de la classe.

(% style="background-color: rgb(204, 204, 204);" %)object = [Class alloc];

I per últim, inicialitzaríem l'objecte amb la funció //init//. Aquest mètode s'encarrega d'executar les instruccions necessàries en crear el nostre objecte per a tenir-lo llest per a utilitzar.

(% style="background-color: rgb(204, 204, 204);" %)object = [object init];

Podem realitzar aquestes tres accions amb una mateixa línia:

(% style="background-color: rgb(204, 204, 204);" %)Class *object = ~[~[Class alloc] init];

D'aquesta manera s'inclou el resultat d'una crida dins d'una altra. El resultat és idèntic, però és més senzilla i curta aquesta última variant.

Els mètodes //alloc //i //init //esmentats són comuns a tots els objectes. Això és així perquè totes les classes hereten d'una classe arrel anomenada NSObject, que disposa d'una sèrie de mètodes comuns a tots els objectes que ens seran de gran utilitat habitualment.

Quan declarem un objecte, aquest té assignat el valor nil, que ve a ser el //null //utilitzat en altres llenguatges de programació. Un cop hàgim demanat la memòria i inicialitzat l'objecte, aquest deixarà de valer nil, i estarà llest per a ser utilitzat.


=== 1.7.3 Pas de paràmetres ===

A continuació veurem com funciona el pas de paràmetres i el retorn de valors. És important notar que en Objective-C no es permet la sobrecàrrega de funcions o //function overloading//. Això vol dir que no hi poden haver dos mètodes amb el mateix nom. A continuació veurem com Objective-C soluciona aquesta carència.

Imaginem-nos, per exemple, un mètode d'una classe anomenada API que ens comprova que hem fet el //login //correctament.

Sense cap paràmetre la declaració seria:

(% style="background-color: rgb(204, 204, 204);" %)+(BOOL)doLogin;

El símbol + del principi ens indica que es tracta d'un mètode de classe (anomenat mètode //static //en altres llenguatges). El símbol –, per altra banda, ens indica que es tracta d'un mètode d'un objecte. A continuació, entre parèntesis, trobem el tipus retornat, en aquest cas un booleà BOOL per a indicar-nos si s'ha fet el //login //correctament. Si el mètode no retorna res, s'utilitza (void).

Per cridar aquest mètode faríem la crida:

(% style="background-color: rgb(204, 204, 204);" %)BOOL result=[API doLogin];

En aquest cas rebrem un booleà amb el resultat de cridar el mètode de classe //doLogin//.

Per poder passar paràmetres a un mètode s'afegeix el símbol :.

En aquest cas passarem una cadena de caràcters, de tipus NSString:

(% style="background-color: rgb(204, 204, 204);" %)+(BOOL)doLoginWithUserName: (NSString*)user;

A la declaració després dels dos punts, s'afegeix entre parèntesis el tipus. Si és un objecte, s'afegeix el símbol de punter * i després s'afegeix el nom del paràmetre.

Per cridar aquest mètode el codi és el següent:

(% style="background-color: rgb(204, 204, 204);" %)BOOL result = [API doLoginWithUserName:username];

Com veiem només hem d'afegir dos punts i la variable que volem enviar. És important notar que el nom del mètode ha canviat; això és habitual en Objective-C, ja que és més entenedor d'aquesta manera:

(% style="background-color: rgb(204, 204, 204);" %)[API doLoginWithUserName:username]

que no pas:

(% style="background-color: rgb(204, 204, 204);" %)[API doLogin:username]

A continuació ho complicarem una mica més, passant dos paràmetres.

La declaració és la següent:

(% style="background-color: rgb(204, 204, 204);" %)+(BOOL)doLoginWithUserName: (NSString *)user andPassword : (NSString *)password;

Quan es passa més d'un paràmetre s'acostuma a afegir un text descriptiu per al segon paràmetre que acompanyi l'acció del mètode, en aquest cas //andPassword//. No és obligatori; per tant, també ho podríem definir així:

(% style="background-color: rgb(204, 204, 204);" %)+(BOOL)doLoginWithUserName: (NSString *)user : (NSString *)password;

En aquest cas, els dos noms de variables que rep la funció són //user //i //password//.

I la crida quedarà:

(% style="background-color: rgb(204, 204, 204);" %)BOOL result = [API doLoginWithUserName:username andPassword:password];

o

(% style="background-color: rgb(204, 204, 204);" %)BOOL result = [API doLoginWithUserName:username :password];

Queda molt més entenedora la primera opció, és per això que s'afegeix aquest nom.

Aquesta manera de treballar amb els paràmetres fa que no es trobi a faltar la possibilitat de sobrecarregar les funcions.

Per tant, podem tenir definits aquests mètodes a la vegada sense problemes:

(% style="background-color: rgb(204, 204, 204);" %)+(BOOL)doLogin;(%%)
(% style="background-color: rgb(204, 204, 204);" %)+(BOOL)doLoginWithUserName: (NSString*)user;(%%)
(% style="background-color: rgb(204, 204, 204);" %)+(BOOL)doLoginWithPassword: (NSString*)password;(%%)
(% style="background-color: rgb(204, 204, 204);" %)+(BOOL)doLoginWithUserName: (NSString *)user andPassword: (NSString *)password;

Pot semblar complicat d'entrada però més endavant ens ajudarà a llegir millor el codi.


=== 1.7.4 Tipus de variables i classes ===

Com hem comentat abans, programant amb Objective-C podem utilitzar per a treballar tant variables de C com int, float, com classes de més alt nivell com NSString o NSNumber, que veurem més endavant.

Però també és important comentar que existeixen uns equivalents als tipus int, uint, float, etc. amb què és més recomanable treballar, que s'anomenen NSInteger, NSUInteger, NSDecimal, etc. Aquests nous tipus s'encarreguen automàticament d'escollir la mida ideal quan es treballa amb una màquina de 32 bits o 64 bits, i per això sempre són més recomanables d'utilitzar.

A continuació presentarem algunes de les classes del //framework //Foundation que utilitzarem més habitualment programant amb el llenguatge Objective-C. N'hi ha moltes, cadascuna per a un objectiu determinat. A continuació veurem les més destacades.


**NSString**
Es tracta d'una classe encarregada del tractament de //strings// immutables. Disposa de molts mètodes per al tractament de //strings //o cadenes de caràcters que ens seran de gran utilitat, mètodes de cerca, comparació, etc.

Per a cadenes de caràcters mutables disposem de la classe que hereta d'NSString i que s'anomena NSMutableString, amb la qual disposarem, a més, de mètodes per a modificar les nostres cadenes de caràcters.

Per definir cadenes de caràcters immutables, amb Objective-C s'utilitza el símbol @ davant de tot de la cadena.

En aquest exemple guardem el text "cadenadetext" dintre la variable cadena.

(% style="background-color: rgb(204, 204, 204);" %)NSString *cadena = @"cadenadetext";

A continuació cridem el metode //length //d'NSString per a obtenir-ne la llargada.

(% style="background-color: rgb(204, 204, 204);" %)NSUInteger llargada = [cadena length];


**NSNumber**
És la classe amb la qual podem emmagatzemar i treballar amb números, ja siguin int, float, NSInteger, etc.

Per crear un objecte NSNumber on guardar el valor 1 faríem el següent:

(% style="background-color: rgb(204, 204, 204);" %)NSNumber *number = [NSNumber numberWithInteger:1];

Pot semblar incòmode realitzar operacions amb aquesta classe, però són classes pensades per homogeneïtzar el tractament dels diferents tipus de variables numèriques i no tant per fer operacions; són classes contenidores de nombres.

Per sumar dos valors s'acostuma a sumar les classes de C i després guardar-ho en un NSNumber:

(% style="background-color: rgb(204, 204, 204);" %) NSInteger a=2, b=3;(%%)
(% style="background-color: rgb(204, 204, 204);" %) NSNumber *resultat = [NSNumber numberWithInteger:a+b];

Per crear un NSNumber nou i donar-li el valor 1 hauríem d'utilitzar les funcions //alloc //i //init//:

(% style="background-color: rgb(204, 204, 204);" %)NSNumber *numero = ~[~[NSNumber alloc] initWithInteger:1];

De totes maneres hi ha excepcions on això no és necessari.

Utilitzar l'//alloc// implicava, abans de la tecnologia ARC, que nosaltres érem els responsables d'aquella variable i, per tant, hauríem de tenir una instrucció //release //per alliberar l'espai. Per facilitar la feina hi havia els mètodes de conveniència en els quals era la mateixa classe la que s'encarregava de gestionar la memòria de la variable. Un exemple és la instrucció que hem vist abans:

(% style="background-color: rgb(204, 204, 204);" %)NSNumber *numero = [NSNumber numberWithInteger:1];

Amb ARC activat, a efectes pràctics les dues sentències són completament idèntiques. Sense ARC la cosa canviaria, ja que hauríem d'alliberar la memòria demanada en el primer cas cridant el mètode //release//. Els mètodes de conveniència, per tant, eren molt útils per no haver-nos de preocupar d'alliberar després les variables.

I darrerament, amb l'última versió del compilador utilitzat per Xcode, es poden utilitzar els anomenats Objective-C literals, que són declaracions molt més curtes que no pas les que hem comentat anteriorment. Les identificarem perquè el primer caràcter és també una @, igual que quan definim cadenes de caràcters.

Les instruccions següents són totes equivalents:

(% style="background-color: rgb(204, 204, 204);" %)NSNumber *numero =@3U;(%%)
(% style="background-color: rgb(204, 204, 204);" %)NSNumber *numero = [NSNumber numberWithUnsignedInteger:3];

(% style="background-color: rgb(204, 204, 204);" %)NSNumber *numero2 =@YES;(%%)
(% style="background-color: rgb(204, 204, 204);" %)NSNumber *numero2 = [NSNumber numberWithBool:YES];


**NSArray**
És la classe encarregada de tractar amb //arrays //o conjunts d'objectes. Ens permet tractar els //arrays //com a piles i cues. Dins d'un NSArray només podem guardar objectes d'Objective-C com NSString, NSNumber, NSDate, etc., no podem emmagatzemar variables de C com int o float.

Per indicar el final d'un NSArray ho farem amb el valor nil. Si necessitéssim indicar una casella buida d'un //array//, podem guardar [NSNull null], ja que si no estaríem indicant el final de l'//array// en aquella casella.

NSArray, de la mateixa manera que NSString, és immutable, cosa per la qual no el podrem modificar. Si volem modificar els seus valors, tenim la classe NSMutableArray, que disposa de mètodes especials per poder modificar els diferents objectes dins de l'//array//.

Alguns exemples de creació d'NSArray serien:

(% style="background-color: rgb(204, 204, 204);" %)NSArray *array = [NSArray arrayWithObjects:a,b,[NSNull null],c,nil];

(% style="background-color: rgb(204, 204, 204);" %)NSArray *array = @[a,b,[NSNull null],c];

(% style="background-color: rgb(204, 204, 204);" %)NSMutableArray *array = ~[~[NSMutableArray alloc] init];


**NSDictionary**

És una classe contenidora d'altres com NSArray, però en un NSDictionary els objectes es guarden amb una clau i no han d'estar necessàriament ordenats. Igual que en les altres classes, també disposem d'una classe mutable, en aquest cas NSMutableDictionary.

Un exemple seria:

(% style="background-color: rgb(204, 204, 204);" %)NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:a, @"clau-a", b, @"claub",nil];

o:

(% style="background-color: rgb(204, 204, 204);" %)NSMutableDictionary *dictionary = @{@"clau-a" : a, @"clau-b" : b };

Això ens guardaria les variables //a //i //b//.

Per consultar //a //només hauríem de fer:

(% style="background-color: rgb(204, 204, 204);" %)NSNumber *a = [dictionary valueForKey:@"clau-a"];

Hi ha moltes més classes disponibles a Foundation. Les podem consultar totes des de la documentació inclosa a l'iOS SDK.


=== 1.7.5 Custom classes ===

Per desenvolupar una aplicació, necessitarem definir les nostres pròpies classes. Les del llenguatge Objective-C necessiten dos fitxers, un primer amb extensió .h amb la declaració i un segon fitxer amb extensió .m amb la implementació.

Al fitxer .h és on trobarem la classe de qui hereta la nostra classe, les variables que utilitza i els mètodes que implementa. Tot va dins de l'element @interface i indiquem el final amb @end.

Un exemple de fitxer d'arxiu .h seria:

(% style="background-color: rgb(204, 204, 204);" %)@interface MyClass : NSObject

(% style="background-color: rgb(204, 204, 204);" %)-(void)methodFunction;(%%)
(% style="background-color: rgb(204, 204, 204);" %)+(void)classFunction;

(% style="background-color: rgb(204, 204, 204);" %)@end

En aquest fragment, MyClass és el nom de la nostra classe, NSObject és la classe de qui heretem, en aquest cas de la classe arrel NSObject. A continuació ens trobem amb la declaració dels mètodes de la nostra classe. Per a ser accessible des d'altres classes haurem de declarar-ho aquí, si no el mètode no serà accessible des de l'exterior.

Notem que un mètode té el signe – i l'altre el +. Això indica, com hem comentat anteriorment, si es tracta d'un mètode d'una classe o és un mètode dels objectes de la classe.

En el cas del mètode de classe, cridaríem el mètode de la manera següent:

(% style="background-color: rgb(204, 204, 204);" %)[MyClass classFunction];

En canvi, per cridar el mètode d'un objecte de la classe, inicialitzaríem primer l'objecte i després ja podríem cridar el mètode.

(% style="background-color: rgb(204, 204, 204);" %)MyClass *myObject = ~[~[MyClass alloc] init];(%%)
(% style="background-color: rgb(204, 204, 204);" %)[myObject methodFunction];

A continuació veurem com afegir variables dins la nostra classe:

(% style="background-color: rgb(204, 204, 204);" %)@interface MyClass : NSObject <MyDelegate>{

(% style="background-color: rgb(204, 204, 204);" %)     NSString *myString;(%%)
(% style="background-color: rgb(204, 204, 204);" %) NSInteger myNumber;(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

(% style="background-color: rgb(204, 204, 204);" %)@property (readwrite, nonatomic) int myNumber;

(% style="background-color: rgb(204, 204, 204);" %)-(void)methodFunction;(%%)
(% style="background-color: rgb(204, 204, 204);" %)+(void)classFunction;(%%)
(% style="background-color: rgb(204, 204, 204);" %)@end

(% style="font-size: 14px; /* background-color: rgb(245, 245, 245); */" %)Les variables que afegim dins de les claus de l'element @interface s'anomenen //instance variables//. Totes són //protected//, que vol dir que seran accessibles des de la mateixa classe i també subclasses. En C++, per defecte, serien privades, amb la qual cosa només serien accessibles des de la mateixa classe però no per a les subclasses.

Per fer-les accessibles de forma externa a qualsevol classe hem de crear una @property, que tenen diferents atributs que veurem més endavant. Quan definim una @property automàticament tindrem dos mètodes per a accedir a la variable: un per consultar-la (getProperty), i l'altre per modificar-la (setProperty). Abans de l'Objective 2.0, s'havia d'afegir també la instrucció @synthesize dins de l'arxiu .m, que creava aquests dos mètodes per a cada propietat. Ara ja no cal, atès que totes les @property tindran aquests mètodes //setters //i //getters //de forma automàtica.

A l'exemple anterior, per tant, tindrem una variable anomenada myString que serà accessible des de les mateixes classe i subclasses, i una segona variable anomenada myNumber que serà accessible des de qualsevol altra classe, directament o utilitzant els mètodes setMyNumber i getMyNumber, creats de forma automàtica, tal com hem comentat, encara que no apareguin a la nostra declaració.

També cal comentar que quan definim una @property, li hem d'indicar entre parèntesis les seves propietats. N'hi ha moltes i no entrarem en detall a explicar-les totes.

El més comú és utilitzar (readwrite,nonatomic) per a variables com int, float, etc., que no són objectes, i les propietats (strong,nonatomic) per a objectes.

Els //delegates //ens indiquen quines funcionalitats implementa la nostra classe. Els tractarem més endavant, però per ara veurem que per declarar-los els ubiquem al costat de la classe de la qual heretem entre els símbols <>.

(% style="background-color: rgb(204, 204, 204);" %)@interface MyClass : NSObject <MyDelegate1,MyDelegate2>{(%%)
(% style="background-color: rgb(204, 204, 204);" %)    (%%)
(% style="background-color: rgb(204, 204, 204);" %)    NSString *myString;(%%)
(% style="background-color: rgb(204, 204, 204);" %)    int myNumber;(%%)
(% style="background-color: rgb(204, 204, 204);" %)}

(% style="background-color: rgb(204, 204, 204);" %)@property (readwrite, nonatomic) int myNumber;

(% style="background-color: rgb(204, 204, 204);" %)-(void)methodFunction;(%%)
(% style="background-color: rgb(204, 204, 204);" %)+(void)classFunction;

(% style="background-color: rgb(204, 204, 204);" %)@end
