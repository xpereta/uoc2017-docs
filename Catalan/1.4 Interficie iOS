== 1.4 Interfície en iOS ==

Des dels seus inicis, el sistema iOS no ha canviat de forma dràstica. La interfície ha anat evolucionant i fent canvis estètics, però la majoria de components són els mateixos des de la primera versió d'iOS. La versió d'iOS 7 és la que més canvis va suposar, la majoria de tipus estètic, ja que es va passar d'un disseny skeuomorf a un disseny molt més pla. Actualment la versió d'iOS fa molt ús de colors plans, transparències i efectes de blur. En el seu moment molta gent es va portar les mans al cap en part perquè Android també feia temps que tenia aquesta estètica, però el temps els ha donat la raó del canvi, i ara totes les aplicacions del sistema comparteixen un mateix //look //i això ajuda a ser més productiu. Aquests canvis poden semblar poca cosa, però suposen també molta feina per als desenvolupadors d'aplicacions, ja que tothom vol tenir les aplicacions com més integrades al disseny del sistema millor, de cara a donar una imatge d'aplicació que està al dia.

=== 1.4.1 Conceptes bàsics d'iOS ===

Hi ha una sèrie de conceptes que hem de conèixer del sistema iOS de cara a dissenyar i desenvolupar les nostres aplicacions. Alguns són conceptes bàsics que potser no considerem importants, però afecten, i molt, a l'hora de fer el disseny de les nostres aplicacions.


**Compatibilitat**

Un concepte important a tenir en compte són els dispositius que volem que siguin compatibles amb la nostra aplicació. Bàsicament, la decisió és si volem donar suport per a iPad o no. Si una app dóna suport a iPhone i iPad en una sola app s'anomena una app universal. Xcode també ens permet donar suport només a iPhones o només a iPads. Amb les noves eines d'Xcode per a adaptar la nostra interfície a diferents pantalles, fer una app universal és més costós però tampoc ho és tant com abans, sempre que la interfície no canviï. Si la interfície canvia significativament a la versió tauleta, sí que suposa força feina.

Quant a compatibilitat també s'ha de tenir en compte les versions d'iOS amb les quals volem ser compatibles. Per sort, iOS gaudeix de bona salut i no pateix la fragmentació d'altres plataformes, per la qual cosa l'habitual és que prop del 80% dels dispositius iOS en ús disposin de l'última versió d'iOS instal·lada. L'habitual, per tant, és donar compatibilitat a la versió actual i si de cas, també, a la versió anterior.


**Orientacions**

Una altra cosa a tenir en compte és que iOS ens permet habilitar diferents orientacions per a la nostra aplicació; per tant, és molt important tenir clar en quines orientacions permetrem a l'usuari fer ús de la nostra app.

Les orientacions suportades per iOS són:

-Portrait.
-Upside down.
-Landscape Left.
-Landscape Right.

Aquestes són les orientacions d'interfície de cara a mostrar la nostra interfície a l'usuari; internament en disposem d'alguna altra com per exemple Face Up o Face Down.

Les eines disponibles a iOS ens ajuden a situar i adaptar els elements en diferents orientacions, però moltes vegades s'han de fer ajustaments manualment, cosa a tenir molt en compte a l'hora de dissenyar una aplicació.


**Gestos tàctils**

Quant a gestos multitàctils, també és important conèixer els diferents gestos que tenim disponibles. Era una de les funcionalitats més característiques dels dispositius iOS.

Tenim el //tap//, que seria quan pitgem sobre la pantalla. Aquest gest també permet reconèixer diferents nombres de //tap//, com el //single tap//, //double tap//, etc. És el gest més comú de tots, encara que no sigui un gest multitàctil.

Després trobem el gest de //pinch//, que s'associa generalment a fer zoom a un element. S'utilitzen dos dits i s'ajunten per disminuir un element o se separen per ampliar-lo. És molt característic, per exemple, per a usar en mapes o imatges.

Tenim el //rotation//, que també requereix dos dits. Amb dos dits sobre la pantalla, es giren per a indicar un moviment de gir d'un element.

També tenim el moviment de //swipe//, que es pot realitzar amb un o més dits. És un moviment de desplaçament horitzontal o vertical per la pantalla, utilitzat per a fer //scroll //o desplaçament quan una pantalla no pot visualitzar tota la informació.

També existeix el gest de //panning //o //dragging//, un gest molt semblant al //swipe //però que s'utilitza per a moure elements. Permet un moviment lliure per la pantalla de cara a desplaçar un element.

Per últim, trobem el //long press//, que ens permet reconèixer quan s'està prement un element durant un temps determinat. Al sistema iOS s'utilitza de vegades per a bloquejar o desbloquejar una acció, com per exemple per a eliminar o moure aplicacions.

A continuació es mostren els gestos que iOS SDK ens permet reconèixer per defecte:

[[image:fig66.jpg]]

Si la nostra aplicació necessités cap altre gest que no vingués definit per defecte, el sistema iOS ens permet crear els nostres propis gestos.

Una cosa molt important quan dissenyem els gestos i les interaccions de la nostra aplicació és tenir present els gestos que iOS té per defecte ja integrats, perquè no volem que hi hagi conflicte entre ells. Per exemple, s'ha de vigilar en posar gestos de //swipe //vertical a la part inferior o superior de la pantalla, ja que iOS els detecta i mostra el panell de control o el de notificacions. Per defecte, iOS també permet activar un //swipe //horitzontal per anar endavant o endarrere a les pantalles de la nostra aplicació, i serà decisió nostra activar-lo o no. També hi ha la possibilitat que en fer //tap //a sobre d'on hi ha l'hora, la nostra aplicació, si disposa de //scroll//, vagi directament a dalt de tot per no haver-ho de fer manualment. Si posem un gest de //tap //a la part superior central, també pot generar conflicte.


**//Status bar//**

Un element que acompanya iOS des de la primera versió és el que s'anomena //status bar//, la barra de la part superior on apareix informació com la cobertura, l'hora o la bateria. Aquesta barra, fins a la versió 7 es podia amagar o mostrar, però no canviar-li l'aspecte; a partir de la versió 7, l'espai que ocupava aquesta barra també s'utilitza a l'aplicació, per la qual cosa el color de fons de la barra dependrà del color de la nostra aplicació.


=== 1.4.2 Components de UIKit ===

La majoria de les apps d'iOS estan construïdes amb els elements de UIKit que proporciona Apple. Les diferents pantalles d'una app convencional són UIViewControllers; per tant, (% style="font-size: 14px; /* background-color: rgb(245, 245, 245); */" %)tindrem un UIViewController per a cada pantalla de la nostra aplicació. Aquest UIViewController és el controlador d'una UIView o Vista.

La UIView del UIViewController la podem imaginar com si es tractés del nostre //canvas //o espai on afegirem els elements d'interfície de la nostra aplicació.

A la part pràctica, més endavant, veurem que els UIViewControllers van associats generalment a una classe que crearem nosaltres per a poder accedir als seus elements des del codi.


**//Controllers //o controladors**

El millor per a fer-nos una idea més clara del que és un controlador és visualitzar un exemple d'estructura habitual d'una aplicació.

[[image:fig63.jpg]]

En aquest esquema veiem tres elements: l'element Screen1 i l'element Screen2 són UIViewControllers, i són, per tant, dues pantalles de la nostra aplicació. El fons de color gris i gris fosc que veiem són les UIView de cada UIViewController, on podrem afegir elements de la nostra interfície.

El primer element, NavigationController, és un element especial. Sembla un UIViewController però no és cap pantalla en si. El podem veure com un element que ens permet crear un fil de navegació dins la nostra app. Afegint un UINavigationController com el primer element, podem enllaçar la resta.

Afegint el UINavigationController a l'inici podem disposar del que s'anomena UINavigationBar a la resta de UIViewControllers. L'espai on diu Screen1 i Screen2 és la nostra barra de navegació. Generalment, a la part central es posa el nom de la pantalla i a la dreta i l'esquerra apareixen els elements de navegació. És la barra amb la qual ens movem de pantalla a pantalla. El UINavigationController també ens pot proporcionar un UIToolbar si en necessitem un, apareix com un rectangle gris a la part inferior als tres elements. El UIToolbar és una barra on podem afegir elements, i normalment s'usa per a indicar accions que es poden realitzar a la pantalla.

Si ens hi fixem, a la part superior també apareix una icona amb la bateria. Aquest espai, que en realitat ocupa tot l'espai de dreta a esquerra, s'anomena //status bar //i té una alçada lògica de 20 px. Amb versions anteriors a iOS 7 la //status bar// no formava part de la nostra aplicació, tot i que podíem amagar-la o mostrar-la. A partir de l'iOS 7 l'espai de la //status bar// va passar a formar part també de la UINavigationBar, amb la qual cosa haurem de tenir el compte si la //status bar// està visible a la nostra aplicació, ja que allà apareixerà informació relativa a la cobertura, l'hora o el nivell de bateria. Depenent del color de fons de la nostra aplicació, canviarem el color dels elements de la //status bar// a blanc o negre perquè siguin visibles.

Un altre ViewController important és el UITableViewController. En comptes de disposar d'una UIView on afegir elements, disposa d'una UITableView. L'objectiu d'una UITableView és llistar una sèrie d'elements anomenats cel·les que es desplacen fent //scroll //vertical, molt pràctic a l'hora de llistar una gran quantitat d'informació. Per un tema d'optimització cada cel·la no és un objecte nou, sinó que habitualment es reutilitzen les cel·les que desapareixen de la pantalla en fer //scroll// i s'actualitzen amb la informació de la cel·la que es mostra. Aquest és un procés intern però que s'ha de tenir en compte quan treballem amb taules.


[[image:fig64.jpg]]

En aquest exemple veiem algunes cel·les habituals que iOS disposa per defecte. Tenen un títol i un subtítol i a la part dreta hi ha el que s'anomena //accessory view//, al qual podem posar diferents elements segons la funció que vulguem donar a la cel·la.

Si necessitem un disseny diferent per a les nostres necessitats, podem escollir com a tipus de cel·la //custom //i definir nosaltres com volem que sigui.

Des de l'iOS 6 també disposem del UICollectionViewController, que és una versió encara més configurable del UITableViewController, que ens permet generar tota mena d'interfícies molt més vistoses visualment que les UITableViewControllers.

Un altre element controlador a tenir en compte és el UITabBarController. Aquest element, de la mateixa manera que el UINavigationController, no és una pantalla en si, sinó un contenidor de ViewControllers. En aquest cas el UITabBarController ens permet navegar per la nostra aplicació utilitzant una UITabBar a la part inferior, on cada una d'elles ens dirigeix a una secció diferent.

Una estructura amb UITabbarController amb dos apartats podria ser la següent:

[[image:fig65.jpg]]

En aquest cas, el UITabBarController té dues seccions definides, cadascuna de les quals apunta a un UINavigationController que crea dos fils de navegació diferents, un per a la primera secció i un altre per a la segona.


**Vistes o UIView**

Com hem comentat anteriorment, els UIViewController diposen d'una UIView que utilitzem de //canvas// per a afegir més elements. Una UIView és l'element bàsic d'interfície en iOS, un requadre per defecte amb el fons blanc. La gràcia de les UIView és que poden contenir altres UIView. Per a afegir elements a una UIView, només l'haurem d'arrossegar a la UIview que desitgem. A continuació veurem elements que podem utilitzar que hereten d'UIViews.


__UILabel__

El component UILabel permet afegir petits textos a la nostra aplicació, i podem utilitzar qualsevol de les tipografies de què ja disposa iOS o afegir la nostra si ho desitgem. Les UILabel poden tenir una o més línies de text; si volem que el nombre de línies s'adapti a la mida disponible, posarem com a nombre de línies el valor de 0.

El UILabel permet definir textos sense atributs o amb atributs. Amb atributs, podrem definir diferents estils dins d'un mateix text.


__UIButton__

Els UIButton ens permeten definir espais interactius dins de la nostra aplicació. Aquests botons, per defecte consten només de text, però podem incloure-hi imatges, tant descriptives com de fons. Se'ns dona la possibilitat de definir diferents configuracions del botó per a diferents estats, com per exemple canviar el color del text quan es prem el botó. Els UIButton també ens permeten definir el tipus d'interacció a detectar com Touch Down Repeat o Touch Drag Inside, tot i que la més comuna és Touch Up Inside, que és la que respon en prémer-hi a sobre.


__UITextField__

Aquest element permet que l'usuari introdueixi textos utilitzant el teclat. Quan l'usuari l'activa es desplega el teclat de forma automàtica. Aquest component ens permet escollir el teclat a desplegar, el tipus de tecla d'acceptació, si es vol mostrar o no el text escrit, etc. És un component imprescindible per a les pantalles d'accés.


__UIImageView__

El component UIImageView ens permet afegir imatges de forma senzilla dins la nostra aplicació. S'utilitza quan aquesta imatge no ha de rebre interacció directa per part de l'usuari, ja que d'altra manera es pot utilitzar el UIButton. UIImageView també ens permet ajustar més fidelment com visualitzar una imatge que no pas amb els UIButtons.


=== 1.4.3 Mides de pantalla ===

Un dels problemes més habituals a l'hora de dissenyar aplicacions mòbils és treballar amb diferents mides de pantalla. iOS inicialment només tenia un format, el 4:3, a una resolució de 320 x 480 px de l'iPhone original, amb la qual cosa la tasca d'adaptar correctament una aplicació era molt fàcil, sobretot comparant-ho amb el món d'Android, on existeixen infinitat de pantalles i models diferents.

Però van sortir nous models d'iOS i la resolució original es va veure doblada amb la sortida de la pantalla retina de l'iPhone 4, que va passar a una resolució de 640 x 960 px. Encara que la resolució passés a ser el doble, això no va afectar essencialment el desenvolupament d'una aplicació, ja que Xcode sempre treballa a resolució no nativa a l'hora de crear les interfícies; així, sempre es dissenyava a 320 x 480 px encara que després es mostrés la pantalla al doble de resolució. L'únic que variava és que a l'hora de visualitzar les imatges, el sistema sempre buscava una versió amb la nomenclatura de @2x que indicava que es tractava d'una imatge retina. No hi havia problemes de compatibilitat, ja que les aplicacions que no s'haguessin adaptat encara a pantalla retina es mostraven a resolució normal.

Amb la sortida de l'iPad, la cosa es va complicar més, però en ser només dos aparells tan diferents, moltes vegades l'app ho era d'iPhone o d'iPad, o si es volia una app universal internament es muntaven dues interfícies per separat. També es va permetre executar aplicacions d'iPhone a l'iPad, ja que aquest inicialment no disposava d'aplicacions (encara es permet avui en dia). Quan s'executa una aplicació d'iPhone a l'iPad apareix a la mida de l'iPhone, però permet executar-la també a mode 2x de cara a poder-la utilitzar més fàcilment.

Amb la sortida de l'iPhone 5 la cosa ja es va complicar una mica més, perquè té un format més allargat per fer-se compatible amb el 16:9, amb una resolució de 640 x 1.136 px. Això canviava no només la resolució anterior sinó també la proporció dels altres iPhones; per tant, Apple va presentar una sèrie d'eines per facilitar la resolució d'aquests ajustaments a l'hora de desenvolupar aplicacions. Es va presentar el sistema AutoResizing, que a grans trets es tracta d'una eina d'Xcode que ens permet indicar com es comportarà cada element quan varia la pantalla.

Aquesta eina era suficient per indicar quan un element havia de quedar-se a la part inferior de la pantalla, ampliar-se, etc., ja que entre l'iPhone 4 i l'iPhone 5 no hi havia tantes diferències, només que l'iPhone 5 era una mica més llarg. Les aplicacions que no estiguessin adaptades a l'iPhone 5 mostrarien unes franges negres a la part superior i inferior i, d'aquesta manera, també es visualitzarien correctament.

Amb la presentació de l'iPhone 6 i l'iPhone 6 Plus, es presentaven dues noves pantalles amb diferent resolució de les actuals: l'iPhone 6 a una resolució de 750 × 1.334 px i l'iPhone 6 Plus amb una pantalla gegant a una resolució de 1.080 × 1.920 px. A causa d'aquestes noves pantalles, era ja indispensable utilitzar una eina anomenada AutoLayout, que havia presentat anteriorment Apple i que, al contrari de l'AutoResizing, que permet indicar com es comporta un element respecte a la pantalla, pot indicar com es comporta un element respecte a un altre element. Funciona indicant una sèrie de normes que s'han de complir per ubicar els elements de la nostra interfície; així, li hem d'indicar prou normes per definir la mida i la posició de cadascun dels elements que vulguem que s'adaptin a les diferents pantalles.

A banda de l'AutoLayout, amb el llançament dels nous iPhone 6 es van presentar les Size Classes, que ens permeten definir diferents regles d'AutoLayout depenent de la mida del nostre dispositiu. Així, ajuntant les dues eines se'ns permet controlar bastant eficientment la col·locació dels elements d'una interfície.

Les Size Classes divideixen les diferents mides de pantalla en Compact, Regular o Any. D'aquesta manera, no definim mai un disseny per a un dispositiu concret, i si en el futur n'apareix un amb característiques similars, aquest serà compatible de forma automàtica.

Amb aquestes eines de desenvolupament d'interfícies que hem explicat estem bastant coberts per un temps, però com veiem, Apple innova tant amb els seus dispositius com amb les eines que ens dóna per a desenvolupar aplicacions, per la qual cosa, mentre Apple presenti nous dispositius, també veurem noves eines de desenvolupament.
